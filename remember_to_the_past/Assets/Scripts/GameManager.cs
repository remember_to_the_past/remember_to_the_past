﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    public static GameManager Instancia = null;
    public GameObject instanciaPesonaje;

	private void Awake()
    {
        if (Instancia == null)
        {
            Instancia = this;
        }
        else if (Instancia != this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
        IniciaPersonaje();
    }

    void IniciaPersonaje()
    {
        if(Movimiento.mov == null)
        {
            Instantiate(instanciaPesonaje);
            
        }
    }
}
