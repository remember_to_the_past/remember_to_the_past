﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Movimiento : MonoBehaviour
{

    Rigidbody2D rb2d;
    public float maxVeloidad;
    public bool isJumping = false;
    public bool collisionWithGround = true;
    public static Movimiento mov = null;
    private bool cambioMundo = false;

    void Awake()
    {
        if (mov == null)
        {
            mov = this;
        }
        else if (mov != this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }

    // Use this for initialization
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown("space") && isJumping == false)
        {
            transform.Translate(Vector3.up * 520 * Time.deltaTime, Space.World);
            isJumping = true;
            collisionWithGround = false;
        }

        if (Input.GetKeyDown("z") && Input.GetAxisRaw("Horizontal") < 0)
        {
            transform.Translate(Vector3.left * 1140 * Time.deltaTime, Space.World);
            collisionWithGround = true;
        }
        else
        {
            if (Input.GetKeyDown("z") && Input.GetAxisRaw("Horizontal") > 0)
            {
                transform.Translate(Vector3.right * 1140 * Time.deltaTime, Space.World);
                collisionWithGround = true;
            }
        }


        float mover = Input.GetAxis("Horizontal");
        rb2d.velocity = new Vector2(mover * maxVeloidad, rb2d.velocity.y);

        if (Input.GetKey("q") && cambioMundo == false)
        {
            SceneManager.LoadScene(1);
            cambioMundo = true;

        }
        else
    if (Input.GetKey("q") && cambioMundo == true)
        {
            SceneManager.LoadScene(0);
            cambioMundo = false;

        }
    }


    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Map")
        {
            isJumping = false;
        }

        if (collision.gameObject.tag == "changeworld")
        {
            Debug.Log("collision");
            SceneManager.LoadScene(1);
        }
    }
}